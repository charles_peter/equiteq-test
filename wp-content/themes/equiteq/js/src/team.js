

$(function () {

    /**
     * filters
     */

    $(document).on('click', '.filters .dropdown-item', (e) => {
        const filterby = $(e.target).data('filter');
        const text = $(e.target).text();
        const targetButton = $(e.target).parents('.dropdown-menu').siblings('button');

        $(targetButton).text(text);
        $(targetButton).data('filterby', filterby);

        let sector = $('.filter-by-sector').data('filterby');
        let location = $('.filter-by-location').data('filterby');

        $('.team .team-box-inner').each((i, el) => {

            let classess = el.classList;

            if (classess.contains(sector) && classess.contains(location)) {
                el.classList.remove('d-none');
            }
            else {
                el.classList.add('d-none');

                if (sector == 'all-sector') {
                    if (classess.contains(location)) {
                        el.classList.remove('d-none');
                    }
                }

                if (location == 'all-location') {
                    if (classess.contains(sector)) {
                        el.classList.remove('d-none');
                    }
                }

                if (sector == 'all-sector' && location == 'all-location') {
                    el.classList.remove('d-none');
                }
            }
        });

        console.log(sector, location);


        /**
         * check of Search <input>
         * - re-run quick name search
         */

        let s = $('#quicksearch').val();
        let foundIndex = [];

        $('.team .team-box-inner').each((i, el) => {

            let classess = el.classList;

            if (!classess.contains('d-none')) {

                let member_name = $(el).find('.member-name').find('a').text();

                if (member_name.toLowerCase().includes(s.toLowerCase())) {
                    foundIndex.push(i);
                    console.log(member_name);
                }
            }
        });

        console.log(foundIndex);

        $('.team .team-box-inner').each((i, el) => {
            el.classList.add('d-none');
            if (foundIndex.includes(i)) {
                el.classList.remove('d-none');
            }
        });

    });

    /**
     * quick search
     */

    $(document).on('keyup', '#quicksearch', (e) => {

        let s = $(e.target).val();

        if (!s.length) {
            $('.team .team-box-inner').removeClass('d-none');

            /**
             * check is filter is ON
             * - re-run the filter
             */

            let sector = $('.filter-by-sector').data('filterby');
            let location = $('.filter-by-location').data('filterby');

            $('.team .team-box-inner').each((i, el) => {

                let classess = el.classList;

                if (classess.contains(sector) && classess.contains(location)) {
                    el.classList.remove('d-none');
                }
                else {
                    el.classList.add('d-none');

                    if (sector == 'all-sector') {
                        if (classess.contains(location)) {
                            el.classList.remove('d-none');
                        }
                    }

                    if (location == 'all-location') {
                        if (classess.contains(sector)) {
                            el.classList.remove('d-none');
                        }
                    }

                    if (sector == 'all-sector' && location == 'all-location') {
                        el.classList.remove('d-none');
                    }
                }
            });

            console.log(sector, location);

            return;
        }

        /**
         * collect search findings by array index 
         */

        let foundIndex = [];

        $('.team .team-box-inner').each((i, el) => {

            let classess = el.classList;

            if (!classess.contains('d-none')) {

                let member_name = $(el).find('.member-name').find('a').text();

                if (member_name.toLowerCase().includes(s.toLowerCase())) {
                    foundIndex.push(i);
                    console.log(member_name);
                }
            }
        });

        console.log(foundIndex);

        $('.team .team-box-inner').each((i, el) => {
            el.classList.add('d-none');
            if (foundIndex.includes(i)) {
                el.classList.remove('d-none');
            }
        });
    });

});