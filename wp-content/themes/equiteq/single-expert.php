<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);

global $post;

$profile = get_field('expert', $post->ID);



?>


<section class="team-single">
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a href="/team" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>


        <!--May implement the expert's profile here -->

        <div class="row">
            <div class="left col-md-3">
                <div class="team-bg-img"><img src="<?= $profile['profile_image']['url'] ?>" alt=""></div>
            </div>
            <div class="right col-md-9">

                <div class="profile-title-in">
                    <h1><?= get_the_title() ?></h1>
                </div>

                <div class="profile-designation">
                    <h6><?= $profile['title'] ?></h6>
                </div>

                <div class="city-title">
                    <p>
                        <i class="fa fa-map-marker fa-lg text-green" aria-hidden="true"></i>
                        <?= $profile['location']->post_title ?>
                    </p>
                </div>

                <div class="social-icons">
                    <ul class="experts-socials p-0">
                        <li><a href="mailto:<?= $profile['email'] ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="tel:<?= $profile['contact_no'] ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                        <li><a href="<?= $profile['linkedin'] ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>

                <div><?= get_the_content() ?></div>
            </div>
        </div>


    </div>
</section>


<!--May implement the expert's industry expertise here -->

<?php

$industry_expertise = [];
if ($profile['industry_expertise']) {
    foreach ($profile['industry_expertise'] as $industry) {

        /**
         * icon
         */

        $icon = get_field('industry', $industry->ID);

        $industry_expertise[] = [
            'post_title' => $industry->post_title,
            'icon' => $icon['icon']['url']
        ];
    }
}


if ($industry_expertise) :


?>

    <section class="expertise">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="container">
                        <div class="d-flex flex-column">
                            <div>
                                <h6><?= __('EXPERTISE') ?></h6>
                            </div>
                            <div class="row justify-content-start align-items-center">
                                <?php foreach ($industry_expertise as $industry) : ?>

                                    <div class="col">
                                        <img src="<?= $industry['icon'] ?>" alt="">
                                        <?= $industry['post_title'] ?>
                                    </div>

                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif ?>




<?php get_footer() ?>