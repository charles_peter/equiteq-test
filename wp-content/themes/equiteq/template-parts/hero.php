<?php

global $post;

$banner_image = get_field('banner_image', $post->ID);

?>



<div class="hero-inner" style="background-position: 50% 50%; background-image: url('<?= $banner_image['url'] ?>');">
    <div class="gradient-layer"></div>
</div>