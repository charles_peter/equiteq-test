<div class="team-box-inner col<?= $args['industry'] ? ' '.$args['industry'] : '' ?><?= $args['location_slug'] ? ' '.$args['location_slug'] : '' ?>">
    <div class="team-img">
        <a href="<?= $args['permalink'] ?>">
            <img src="<?= $args['profile_image']['url'] ?>" alt="<?= $args['name'] ?>">
        </a>
    </div>
    <div class="member-name">
        <a href="<?= $args['permalink'] ?>"><?= $args['name'] ?></a>
    </div>
    <div class="member-desigantion"><?= $args['title'] ?></div>
    <div class="member-address"><?= $args['location'] ?></div>
    <div class="social-icons">
        <ul class="experts-socials p-0">
            <li><a href="mailto:<?= $args['email'] ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
            <li><a href="tel:<?= $args['contact_no'] ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
            <li><a href="<?= $args['linkedin'] ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>