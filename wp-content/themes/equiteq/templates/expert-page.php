<?php
/* Template Name: Expert Page */
get_header();
$id = get_the_ID();
$page = get_post($id);

/**
 * intro title
 */

$intro_title = get_field('intro_title', $id);


/**
 * industry
 */

$industry_query = new WP_Query([
    'post_type' => 'industry',
]);

wp_reset_postdata();

$industries = $industry_query->posts;


/**
 * location
 */

$location_query = new WP_Query([
    'post_type' => 'location',
]);

wp_reset_postdata();

$locations = $location_query->posts;

/**
 * Hero
 */

hm_get_template_part('template-parts/hero', ['page' => $page]);

?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4"><?php echo $intro_title ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?= get_the_content() ?>
            </div>
        </div>

        <!--May implement the search and filter here-->

        <div class="row">

            <div class="filters col d-flex flex-column">

                <div>
                    <h5><?= __('FILTERS') ?></h5>
                </div>

                <div class="d-flex">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle filter-by-sector" data-filterby="all-sector" type="button" data-toggle="dropdown">
                            <?= __('Sector') ?>
                        </button>
                        <div class="dropdown-menu">
                            <button class="dropdown-item" type="button" data-filter="all-sector">All</button>

                            <?php foreach ($industries as $industry) : ?>
                                <button class="dropdown-item" type="button" data-filter="<?= $industry->post_name ?>"><?= get_the_title($industry->ID) ?></button>
                            <?php endforeach ?>

                        </div>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle filter-by-location" data-filterby="all-location" type="button" data-toggle="dropdown">
                            <?= __('Location') ?>
                        </button>
                        <div class="dropdown-menu">
                            <button class="dropdown-item" type="button" data-filter="all-location">All</button>

                            <?php foreach ($locations as $location) : ?>
                                <button class="dropdown-item" type="button" data-filter="<?= $location->post_name ?>"><?= get_the_title($location->ID) ?></button>
                            <?php endforeach ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="search col">
                <h5><?= __('SEARCH') ?></h5>
                <input type="text" class="" id="quicksearch">
            </div>

        </div>
    </div>
</section>

<!-- May implement the experts profile list here -->

<section class="team">
    <div class="container">
        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">

            <?php

            $query = new WP_Query([
                'post_type' => 'expert',
                'orderby' => 'meta_value_num',
                'meta_key' => 'expert_list_order',
                'order' => 'ASC'
            ]);

            wp_reset_postdata();

            $members = $query->posts;

            foreach ($members as $member) {
                $profile = get_field('expert', $member->ID);
                $name = get_the_title($member->ID);
                $permalink = get_the_permalink($member->ID);
                $location = $profile['location']->post_title;
                $location_slug = $profile['location']->post_name;

                $profile['name'] = $name;
                $profile['permalink'] = $permalink;
                $profile['location'] = $location;
                $profile['location_slug'] = $location_slug;

                $industry_expertise = [];
                if ($profile['industry_expertise']) {
                    foreach ($profile['industry_expertise'] as $industry) {
                        $industry_expertise[] = $industry->post_name;
                    }
                }

                if ($industry_expertise) {
                    $industry_expertise_slugs = implode(' ', $industry_expertise);
                    $profile['industry'] = $industry_expertise_slugs;
                }

                get_template_part('template-parts/team', null, $profile);
            }

            ?>

        </div>
    </div>
</section>

<?php get_footer() ?>